﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace Homework_Theme_01
{
    class Program
    {
        static void Main(string[] args)
        {
            /// <summary>
            /// Базовые переменные
            /// </summary>
            string Name = "John"; //имя
            int Age = 19; //возраст
            double Height = 1.8; // рост

            /// <summary>
            /// Переменные для баллов по предметам и среднего балла
            /// </summary>
            double MathScore = 3.0; // математика
            double RusScore = 3.0; // русский язык
            double HistoryScore = 5.0; // история
            double MiddleScore = (MathScore + RusScore + HistoryScore) / 3;

            const int lineCount2 = 8;
            var headerTemplate = "Способ 01";
            var nameTemplate = $"Имя: {Name}";
            var ageTemplate = $"Возраст: {Age}";
            var RusTemplate = $"Балл по русскому языку: {RusScore}";
            var HisTemplate = $"Балл по истории: {HistoryScore}";
            var MathTemplate = $"Балл по математике: {MathScore}";
            var MiddleTemplate = $"Средний балл: {MiddleScore}";
            var windowWidth = Console.WindowWidth;
            var windowHeight = Console.WindowHeight;
            Console.SetCursorPosition((windowWidth - headerTemplate.Length) / 2, (windowHeight - lineCount2) / 2);
            Console.WriteLine(headerTemplate);
            Console.SetCursorPosition((windowWidth - nameTemplate.Length) / 2, Console.CursorTop);
            Console.WriteLine(nameTemplate);
            Console.SetCursorPosition((windowWidth - ageTemplate.Length) / 2, Console.CursorTop);
            Console.WriteLine(ageTemplate);
            Console.SetCursorPosition((windowWidth - RusTemplate.Length) / 2, Console.CursorTop);
            Console.WriteLine(RusTemplate);
            Console.SetCursorPosition((windowWidth - HisTemplate.Length) / 2, Console.CursorTop);
            Console.WriteLine(HisTemplate);
            Console.SetCursorPosition((windowWidth - MathTemplate.Length) / 2, Console.CursorTop);
            Console.WriteLine(MathTemplate);
            Console.SetCursorPosition((windowWidth - MiddleTemplate.Length) / 2, Console.CursorTop);
            Console.WriteLine(MiddleTemplate);
            Console.ReadLine(); //Нажать клавишу чтобы продолжить

            /// <summary>
            /// Базовый ввод данных в консоль
            /// </summary>
            Console.Clear();
            Console.WriteLine(Name
                + Age
                + Height
                + MathScore
                + RusScore
                + HistoryScore
                + MiddleScore);
            Console.ReadLine(); //Нажать клавишу чтобы продолжить

            /// <summary>
            /// Вывод форматированных строк
            /// </summary>
            
            string pattern = "Имя: {0} " +
                "Возраст: {1} " +
                "Рост: {2} " +
                "Баллы по математике: {3} " +
                "Баллы по русскому языку: {4} " +
                "Баллы по истории: {5} " +
                "Средний балл: {6} ";
            
            Console.WriteLine(pattern, Name, Age, Height, MathScore, RusScore, HistoryScore, MiddleScore); //вывод данных
            Console.ReadLine(); //Нажать клавишу чтобы продолжить

            /// <summary>
            /// Вывод строк с интерполяцией
            /// </summary>
            Console.Clear();


            Console.WriteLine($"Имя: {Name} " +
              $"Возраст: {Age} " +
              $"Рост: {Height} " +
              $"Баллы по математике: {MathScore} " +
              $"Баллы по русскому языку: {RusScore} " +
              $"Баллы по истории: {HistoryScore} " +
              $"Средний балл: {MiddleScore} "); //вывод данных
            Console.SetCursorPosition(Console.WindowWidth / 2, Console.WindowHeight / 2);
            Console.ReadLine(); //Нажать клавишу чтобы продолжить

            // Заказчик просит написать программу «Записная книжка». Оплата фиксированная - $ 120.

            // В данной программе, должна быть возможность изменения значений нескольких переменных для того,
            // чтобы персонифецировать вывод данных, под конкретного пользователя.

            // Для этого нужно: 
            // 1. Создать несколько переменных разных типов, в которых могут храниться данные
            //    - имя;
            //    - возраст;
            //    - рост;
            //    - баллы по трем предметам: история, математика, русский язык;

            // 2. Реализовать в системе автоматический подсчёт среднего балла по трем предметам, 
            //    указанным в пункте 1.

            // 3. Реализовать возможность печатки информации на консоли при помощи 
            //    - обычного вывода;
            //    - форматированного вывода;
            //    - использования интерполяции строк;

            // 4. Весь код должен быть откомментирован с использованием обычных и хml-комментариев

            // **
            // 5. В качестве бонусной части, за дополнительную оплату $50, заказчик просит реализовать 
            //    возможность вывода данных в центре консоли.



        }
    }
}
